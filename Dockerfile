FROM alpine:3.8
RUN apk update && apk add bash curl && apk add bash jq
CMD /bin/bash
# RUN pip install -U tox
COPY pipe /usr/bin/
RUN chmod +x /usr/bin/pipe.sh
ENTRYPOINT ["/usr/bin/pipe.sh"]
