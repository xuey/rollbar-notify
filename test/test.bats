#!/usr/bin/env bats

# NOTE: This test assumes that ROLLBAR_ACCESS_TOKEN has `post_server_item`
# as well as `read` scopes.

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/rollbar-notify"}

  echo "Building image $DOCKER_IMAGE..."
  docker build -t ${DOCKER_IMAGE} .
}

@test "Deploy is successfully posted" {

    run docker run \
        -e ROLLBAR_ACCESS_TOKEN="${ROLLBAR_ACCESS_TOKEN}" \
        -e ROLLBAR_ENVIRONMENT="test" \
        -e BITBUCKET_COMMIT="${BITBUCKET_COMMIT}" \
        -e BITBUCKET_REPO_OWNER="${BITBUCKET_REPO_OWNER}" \
        -e BITBUCKET_REPO_SLUG="${BITBUCKET_REPO_SLUG}" \
        -e BITBUCKET_BUILD_NUMBER="${BITBUCKET_BUILD_NUMBER}" \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    # Checking for status 0 is a sufficient test because this only happens if
    # a deploy was successfully POSTed.
    [ "$status" -eq 0 ]

    # Hit the Rollbar API to make sure that the deploy exists
    # URL="https://api.rollbar.com/api/1/deploy/${output}?access_token=${BITBUCKET_BUILD_NUMBER}"
    # response=`curl --request GET \
    #     --url $URL`
    # echo "Status: $status"
    # [ "$status" -eq 0 ]
}
